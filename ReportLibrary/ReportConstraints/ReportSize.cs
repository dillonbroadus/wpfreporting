﻿namespace ReportLibrary.ReportConstraints
{
    public class ReportSize
    {
        public double InHeight { get; }
        public double InWidth { get; }

        public double DpiHeight { get; }
        public double DpiWidth { get; }

        public ReportSize(double inWidth, double inHeight)
        {
            InWidth = inWidth;
            InHeight = inHeight;
            DpiHeight = 96.0 * inHeight;
            DpiWidth = 96.0 * inWidth;
        }

        public static readonly ReportSize A4 = new ReportSize(8.3, 11.7);

        public static readonly ReportSize Letter = new ReportSize(8.5, 11);

        public static readonly ReportSize Legal = new ReportSize(8.5, 14);

        public static readonly ReportSize Foolscap = new ReportSize(8, 13);
    }
}