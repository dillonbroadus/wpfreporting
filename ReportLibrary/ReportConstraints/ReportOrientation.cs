﻿namespace ReportLibrary.ReportConstraints
{
    public enum ReportOrientation
    {
        Portrait,
        Landscape
    }
}