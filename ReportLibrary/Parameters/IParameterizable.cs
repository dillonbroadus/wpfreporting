﻿using System;

namespace ReportLibrary.Parameters
{
    public interface IParameterizable
    {
        Type ParamType { get; }

        void InjectParameters(object param);
    }
}