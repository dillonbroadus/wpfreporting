﻿using System;
using System.Windows;

namespace ReportLibrary.Parameters
{
    /// <summary>
    /// Interaction logic for GenericTextInput.xaml
    /// </summary>
    internal partial class GenericTextInput : IReturnValue
    {
        public GenericTextInput()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty FieldNameProperty = DependencyProperty.Register(
            "FieldName", typeof(string), typeof(GenericTextInput), new PropertyMetadata(default(string)));

        public string FieldName
        {
            get => (string)GetValue(FieldNameProperty);
            set => SetValue(FieldNameProperty, value);
        }

        public virtual object GetValue()
        {
            throw new NotImplementedException("GetValue logic must be implemented in subclasses of GenericTextInput");
        }
    }

    internal class GenericTextInput<T> : GenericTextInput
    {
        public override object GetValue()
        {
            try
            {
                return Convert.ChangeType(Input.Text, typeof(T));
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}