﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace ReportLibrary.Parameters
{
    /// <summary>
    /// Interaction logic for ParameterEntry.xaml
    /// </summary>
    public partial class ParameterEntry : Window
    {
        public static Dictionary<Type, Func<IReturnValue>> FieldComponentRegistry =
            new Dictionary<Type, Func<IReturnValue>>
            {
                {typeof(int), () => new GenericTextInput<int>()}
            };

        public ParameterEntry()
        {
            InitializeComponent();
        }

        public ParameterEntry(Type type) : this()
        {
            SetupParameterEntry(type);
        }

        private void SetupParameterEntry(Type type)
        {
            foreach (var prop in type.GetProperties())
            {
                var fieldControl = FieldComponentRegistry[prop.PropertyType]();
                fieldControl.FieldName = prop.Name;
                ParameterFields.Children.Add((UIElement)fieldControl);
            }
        }

        public object GetParamObject(Type type)
        {
            var obj = Activator.CreateInstance(type);
            var props = type.GetProperties();

            for (var i = 0; i < props.Length; i++)
            {
                props[i].SetValue(obj, ((IReturnValue)ParameterFields.Children[i]).GetValue());
            }

            return obj;
        }

        public static object Show(Type type)
        {
            var pe = new ParameterEntry(type);
            pe.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            pe.ShowDialog();
            return pe.GetParamObject(type);
        }

        private void DoIt(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }

    public interface IReturnValue
    {
        string FieldName { set; }

        object GetValue();
    }
}