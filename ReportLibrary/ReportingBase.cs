﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using ReportLibrary.ReportConstraints;

namespace ReportLibrary
{
    [ContentProperty("DetailContent")]
    public abstract class ReportingBase : Control
    {
        #region Size

        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register(
            "Size", typeof(ReportSize), typeof(ReportingBase), new PropertyMetadata(ReportSize.A4));

        public ReportSize Size
        {
            get => (ReportSize)GetValue(SizeProperty);
            set => SetValue(SizeProperty, value);
        }

        #endregion Size

        #region Orientation

        public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register(
            "Orientation", typeof(ReportOrientation), typeof(ReportingBase), new PropertyMetadata(ReportOrientation.Portrait));

        public ReportOrientation Orientation
        {
            get => (ReportOrientation)GetValue(OrientationProperty);
            set => SetValue(OrientationProperty, value);
        }

        #endregion Orientation

        #region Headers

        public static readonly DependencyProperty ReportHeaderProperty = DependencyProperty.Register(
            "ReportHeader", typeof(object), typeof(ReportingBase), null);

        public object ReportHeader
        {
            get => GetValue(ReportHeaderProperty);
            set => SetValue(ReportHeaderProperty, value);
        }

        public static readonly DependencyProperty PageHeaderProperty = DependencyProperty.Register(
            "PageHeader", typeof(object), typeof(ReportingBase), null);

        public object PageHeader
        {
            get => GetValue(PageHeaderProperty);
            set => SetValue(PageHeaderProperty, value);
        }

        #endregion Headers

        #region Main Content

        public static readonly DependencyProperty DetailContentProperty = DependencyProperty.Register(
            "DetailContent", typeof(object), typeof(ReportingBase), null);

        public object DetailContent
        {
            get => GetValue(DetailContentProperty);
            set => SetValue(DetailContentProperty, value);
        }

        #endregion Main Content

        #region Footers

        public static readonly DependencyProperty PageFooterProperty = DependencyProperty.Register(
            "PageFooter", typeof(object), typeof(ReportingBase), null);

        public object PageFooter
        {
            get => GetValue(PageFooterProperty);
            set => SetValue(PageFooterProperty, value);
        }

        public static readonly DependencyProperty ReportFooterProperty = DependencyProperty.Register(
            "ReportFooter", typeof(object), typeof(ReportingBase), null);

        public object ReportFooter
        {
            get => GetValue(ReportFooterProperty);
            set => SetValue(ReportFooterProperty, value);
        }

        #endregion Footers

        static ReportingBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ReportingBase), new FrameworkPropertyMetadata(typeof(ReportingBase)));
        }
    }
}