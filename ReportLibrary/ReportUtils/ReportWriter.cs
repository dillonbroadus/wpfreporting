﻿using System.IO;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Markup;
using ReportLibrary.ReportConstraints;
using FixedDocument = System.Windows.Documents.FixedDocument;
using FixedPage = System.Windows.Documents.FixedPage;
using Point = System.Windows.Point;
using Rect = System.Windows.Rect;
using Size = System.Windows.Size;
using XpsDocument = System.Windows.Xps.Packaging.XpsDocument;

namespace ReportLibrary.ReportUtils
{
    public static class ReportWriter
    {
        public static void WriteReport(string reportName, ReportingBase control)
        {
            var doc = CreateFixedDocument(control);
            var xpsd = new XpsDocument(reportName + ".xps", FileAccess.ReadWrite);
            var xw = XpsDocument.CreateXpsDocumentWriter(xpsd);
            xw.Write(doc);
            xpsd.Close();
            PdfSharp.Xps.XpsConverter.Convert(reportName + ".xps");
            File.Delete(reportName + ".xps");
        }

        public static FixedDocument CreateFixedDocument(ReportingBase control)
        {
            var doc = new FixedDocument();
            doc.DocumentPaginator.PageSize = control.Orientation == ReportOrientation.Portrait
                ? new Size(control.Size.DpiWidth, control.Size.DpiHeight)
                : new Size(control.Size.DpiHeight, control.Size.DpiWidth);

            var page = new PageContent();
            var fixedPage = CreateOneFixedPage(control);
            ((IAddChild)page).AddChild(fixedPage);

            doc.Pages.Add(page);

            return doc;
        }

        public static FixedPage CreateOneFixedPage(ReportingBase control)
        {
            var page = new FixedPage
            {
                Width = control.Orientation == ReportOrientation.Portrait ? control.Size.DpiWidth : control.Size.DpiHeight,
                Height = control.Orientation == ReportOrientation.Portrait ? control.Size.DpiHeight : control.Size.DpiWidth,
                Margin = new Thickness(0)
            };

            FixedPage.SetLeft(control, 0);
            FixedPage.SetTop(control, 0);
            page.Children.Add(control);

            var sz = control.Orientation == ReportOrientation.Portrait
                ? new Size(control.Size.DpiWidth, control.Size.DpiHeight)
                : new Size(control.Size.DpiHeight, control.Size.DpiWidth);
            page.Measure(sz);
            page.Arrange(new Rect(new Point(), sz));
            page.UpdateLayout();

            return page;
        }
    }
}