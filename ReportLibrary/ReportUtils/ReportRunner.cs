﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ReportLibrary.Parameters;

namespace ReportLibrary.ReportUtils
{
    public static class ReportRunner
    {
        public static ReportingBase RunReport<T>() where T : ReportingBase
        {
            return RunReport(typeof(T));
        }

        public static ReportingBase RunReport(Type T)
        {
            var rpt = (ReportingBase)Activator.CreateInstance(T);

            var wind = new Invisible();
            wind.LoadAnd(rpt, () => InjectParameters(rpt));

            return rpt;
        }

        private static void InjectParameters(ReportingBase rpt)
        {
            var paramInterface = rpt.GetType().GetInterfaces().Where(x => x == typeof(IParameterizable)).ToList();

            if (!paramInterface.Any()) return;
            var convRpt = (IParameterizable)rpt;
            var param = Parameters.ParameterEntry.Show(convRpt.ParamType);
            convRpt.InjectParameters(param);
        }

        private class Invisible : Window
        {
            public void LoadAnd(ReportingBase control, Action doBeforeClose)
            {
                Width = 0;
                Height = 0;
                WindowStyle = WindowStyle.None;
                ShowInTaskbar = false;
                ShowActivated = false;
                ResizeMode = ResizeMode.NoResize;
                var brdr = new Border { Child = control };
                AddChild(brdr);
                Show();
                doBeforeClose();
                brdr.Child = null;
                Close();
            }
        }
    }
}