﻿using System;
using System.Windows;
using ReportLibrary.Parameters;

namespace WpfReporting
{
    /// <summary>
    /// Interaction logic for MyCustomReport.xaml
    /// </summary>
    public partial class MyCustomReport : IParameterizable
    {
        public static readonly DependencyProperty ParamsProperty = DependencyProperty.Register(
            "Params", typeof(MyParams), typeof(MyCustomReport), new PropertyMetadata(null));

        public MyParams Params
        {
            get => (MyParams)GetValue(ParamsProperty);
            set => SetValue(ParamsProperty, value);
        }

        public MyCustomReport()
        {
            InitializeComponent();
        }

        public Type ParamType => typeof(MyParams);

        public void InjectParameters(object param)
        {
            Params = (MyParams)param;
        }
    }

    public class MyParams
    {
        public int SomeNum { get; set; } = 28;
    }
}