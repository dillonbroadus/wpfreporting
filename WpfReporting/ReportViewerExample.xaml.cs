﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using ReportLibrary;
using ReportLibrary.ReportUtils;

namespace WpfReporting
{
    /// <summary>
    /// Interaction logic for PaginationExample.xaml
    /// </summary>
    public partial class PaginationExample : Window
    {
        public static readonly DependencyProperty ReportsListProperty = DependencyProperty.Register(
            "ReportsList", typeof(List<Type>), typeof(PaginationExample), new PropertyMetadata(default(List<Type>)));

        public List<Type> ReportsList
        {
            get => (List<Type>)GetValue(ReportsListProperty);
            set => SetValue(ReportsListProperty, value);
        }

        public PaginationExample()
        {
            InitializeComponent();

            ReportsList = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(x => x.IsSubclassOf(typeof(ReportingBase)))
                .ToList();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //var reportName = "something";
            //ReportWriter.WriteReport(reportName, new MyCustomReport());
            //Process.Start(reportName + ".pdf");

            if (Reports.SelectedIndex <= -1) return;

            ReportViewer.Document = ReportWriter.CreateFixedDocument(ReportRunner.RunReport((Type)Reports.SelectedValue));
        }
    }
}